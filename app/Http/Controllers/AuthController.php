<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome(Request $request) {
        // dd($request->all());
        $data = $request->all();
        $fname = strtoupper( $data ["fname"]);
        $lname = strtoupper( $data ["lname"]);

        return view ('welcome', compact('fname','lname'));
    }
    
    public function register() {
        return view('register');
    }
}


// Router::get('/sapa/{nama}', function ($nama) {

//     return "Halo $nama, Selamat Datang!";
// });

