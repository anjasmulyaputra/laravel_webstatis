<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });


// Route::get('/register', function () {
//     return view('register');
// });


// Route::get('/welcome', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@home');

Route::post('/welcome', 'AuthController@welcome')->name('welcome');

Route::get('/register', 'AuthController@register')->name('register');



// Route::get('/hitung/{angka1}/tambah/{angka2}', function ($angka1, $angka2){
//     $hasil = $angka1 + $angka2;

//     return $hasil;
// });

// Router::get('/sapa/{nama}', function ($nama) {

//     return "Halo $nama, Selamat Datang!";
// });
